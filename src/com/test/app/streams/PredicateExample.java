package com.test.app.streams;

import java.util.ArrayList;
import java.util.List;
import java.util.function.Predicate;

public class PredicateExample {
    public List<Apple> apples = new ArrayList<>();
    AppleWeightPredicate appleWeightPredicate;
    AppleColorPredicate appleColorPredicate;

    PredicateExample(){
        apples.add(new Apple(130.0, "red"));
        apples.add(new Apple(150.0, "red"));
        apples.add(new Apple(160.0, "green"));
        apples.add(new Apple(170.0, "red"));
        apples.add(new Apple(190.0, "green"));

        this.appleColorPredicate = new AppleColorPredicate();
        this.appleWeightPredicate = new AppleWeightPredicate();
    }

    public List<Apple> filterByPredicate(Predicate<Apple> predicate){
        List<Apple> filteredApples = new ArrayList<>();
        for(Apple apple : this.apples){
            if(predicate.test(apple)){
                filteredApples.add(apple);
            }
        }

        return filteredApples;
    }

    public class AppleWeightPredicate implements Predicate<Apple> {

        @Override
        public boolean test(Apple apple) {
            return 150 < apple.weight;
        }
    }

    public class AppleColorPredicate implements Predicate<Apple> {

        @Override
        public boolean test(Apple apple) {
            return "red".equals(apple.color);
        }
    }

    public class Apple{
        Double weight;
        String color;

        public Apple(Double weight, String color){
            this.weight = weight;
            this.color = color;
        }

        @Override
        public String toString(){
            return "Weight: "+weight + " Color:" + color;
        }

    }
}
