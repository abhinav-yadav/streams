package com.test.app.streams;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;
import java.util.concurrent.Callable;

public class SampleLambda {

    Comparator<String> comparator = (String s1, String s2) -> s2.compareTo(s1);
    Comparator<String> methodReferenceComparator = String::compareTo;

    /**
     * The type is figured out by the assignment of comparator variable
     * This method is simply using it and calling
     * @param s1
     * @param s2
     * @return
     */
    public Integer getComparatorResult(String s1, String s2){
        System.out.println("Passed to comparator: "+s1+" & "+s2);
        return comparator.compare(s1, s2);
    }

    /**
     * Method references
     *
     * @param s1
     * @param s2
     * @return
     */
    public Integer getComparatorResultUsingMethodRef(String s1, String s2){
        System.out.println("Passed to comparator: "+s1+" & "+s2);
        return methodReferenceComparator.compare(s1, s2);
    }

    public Callable<String> getPerfectCallable(){
        return ()-> "Perfect answer is "+42;
    }

    /**
     * This simply constructs the Callable and returns is it.
     * Also prints what is passed into it.
     * @return
     */
    public Comparator<String> getAnotherComparator(){
        return (String s1, String s2) -> {
            System.out.println("Passed to comparator: "+s1+" & "+s2);
            return s1.compareTo(s2);
        };
    }

    public void getBestComparator(String S1, String S2){
        Comparator<String> c1 = (String s1,String s2)-> s1.compareTo(s2);
        Comparator<String> c2 = Comparator.comparing(String::toString);
        System.out.println("Comparator 1: "+ c1.compare(S1, S2) + " Comparator 2: "+ c2.compare(S1, S2));
    }


    /**
     * This proves the variable is scoping is lexical - Basically
     * whatever is available in enclosing context, is available to Lambda
     * @param name
     * @return
     */
    Callable<String> helloCallable(String name) {
        String hello = "Hello";
        return () -> (hello + ", " + name);
    }

    public void testSample(){

        List<Person> people = new ArrayList<>();
        Collections.sort(people, new Comparator<Person>() {
            public int compare(Person x, Person y) {
                return x.getLastName().compareTo(y.getLastName());
            }
        });

        people.stream().sorted(Comparator.comparing(Person::getLastName));

    }

    public class Person{
        String lastName;

        public Person(String lastName){
            this.lastName = lastName;
        }

        public String getLastName(){
            return lastName;
        }
    }
}
