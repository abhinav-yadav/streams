package com.test.app.streams;

import java.util.Arrays;
import java.util.Comparator;
import java.util.List;
import java.util.Optional;

import com.test.app.streams.PredicateExample.Apple;

import static java.util.Arrays.asList;
import static java.util.Comparator.comparing;
import static java.util.stream.Collectors.toList;

// Behaviour parameterization
public class JavaInActionMain {
    public static void main(String[] arr){
        PredicateExample predicateExample = new PredicateExample();

        List<PredicateExample.Apple> filteredColorApples = predicateExample.
                filterByPredicate(predicateExample.appleColorPredicate);

        List<PredicateExample.Apple> filteredWeightApples = predicateExample.
                filterByPredicate(predicateExample.appleWeightPredicate);

        System.out.println("Color list: "+ filteredColorApples);
        System.out.println("Weight list: "+ filteredWeightApples);

        // Better code with Lambda expression
        List<PredicateExample.Apple> filteredColorApplesL = predicateExample.
                filterByPredicate((PredicateExample.Apple apple) -> "green".equals(apple.color));
        System.out.println("Color list: "+ filteredColorApplesL);

        // Comparator Lambda
        Comparator<PredicateExample.Apple> comparator =
                (Apple a1, Apple a2 ) -> {
                            System.out.println("A1: "+a1 + " A2: "+a2);
                            return a1.weight.compareTo(a2.weight);
                    };

        // Function example to map
        List<Integer> listLength = FunctionExample.map(asList("Abhinav", "Yadav"),
                (String s)->s.length());
        System.out.println("Mapping function: "+listLength);

        // Lambda with method reference
        List<Integer> listLengthWithMethodRef =
                FunctionExample.map(asList("Abhinav", "Yadav"), String::length);
        System.out.println("Mapping function: "+listLengthWithMethodRef);

        // Stream with Lambda
        List<Integer> listLengthWithLambda = asList("Abhinav", "Yadav")
                                            .stream()
                                            .map(String::length)
                                            .collect(toList());
        System.out.println("Mapping function: "+listLengthWithLambda);

        // Stream with reducer
        Optional<Integer> maxValue = asList("Abhinav", "Yadav")
                .stream()
                .map(String::length)
                .reduce(Integer::max);
        System.out.println("Max length: "+maxValue.orElse(0));

        // Count the number of letters
        Long count = asList("Abhinav", "Yadav", "Shivani", "Desai", "Plus One")
                .stream()
                .map((String s)-> s.split(""))
                .flatMap(Arrays::stream)
                .count();
        System.out.println("Count: "+count);
    }
}
