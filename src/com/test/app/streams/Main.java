package com.test.app.streams;

import java.util.stream.Stream;

public class Main {

    public static void main(String[] args) {
        /*Integer[] arrInt = {90, 10, 20, 50 ,40, 40};

        System.out.println("Original array ");
        Stream.of(arrInt)
        .forEach(x ->System.out.println(""+x));

        SimpleStreamOps simpleOps = new SimpleStreamOps();
        simpleOps.printUniqueSortedNumbers(arrInt);
        simpleOps.printDoubleNumbers(arrInt);
        simpleOps.findNumber(arrInt, 40);
        simpleOps.filterAndDoubleNumbers(arrInt);

        System.out.println("Greater char: "+("A".compareTo("a")));*/


        try {
            SampleLambda sampleLambda = new SampleLambda();

            printLambda(sampleLambda.getComparatorResult("a", "b").toString());

            printLambda(sampleLambda.getComparatorResultUsingMethodRef("a", "b").toString());

            printLambda(sampleLambda.helloCallable("World").call());

            printLambda(sampleLambda.getPerfectCallable().call());

            printLambda(""+sampleLambda.getAnotherComparator().compare("a", "b"));

            sampleLambda.getBestComparator("a", "b");

        } catch(Exception e) {
            System.out.println("Some exception: " + e);
        }

    }

    private static void printLambda(String str){
        System.out.println("Lambda Says: "+str);
    }


}
