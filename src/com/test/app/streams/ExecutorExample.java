package com.test.app.streams;

import java.util.Date;
import java.util.List;
import java.util.concurrent.*;

/**
 * Sample completion service implementation - Millennium Mgmt bid
 */
public class ExecutorExample {
    BlockingQueue<Future<Integer>> completionQueue = new ArrayBlockingQueue<>(10);
    CompletionService completionService = new ExecutorCompletionService(Executors.newFixedThreadPool(5),
            completionQueue);

    public void testCompletionService(List<Callable<Integer>> callArray){
        Future<Integer>[] futureArray = new Future[callArray.size()];
        int i=0;
        for(Callable task: callArray){
            futureArray[i] = completionService.submit(task);
            i++;
        }
        try {
            for(Future future:futureArray)
                System.out.println("1 "+future.get());
        } catch (InterruptedException | ExecutionException e) {
            e.printStackTrace();
        }


        /*try {
            for(Future future:futureArray)
                System.out.println("2 "+completionQueue.take().get());
        } catch (InterruptedException | ExecutionException e) {
            e.printStackTrace();
        }*/

        try {
            for(Future future:futureArray) {
                Future result = completionService.take();
                System.out.println("3 "+result);
                System.out.println("3 "+result.get());
            }
        } catch (InterruptedException | ExecutionException e) {
            e.printStackTrace();
        }

    }

    public class TestCallable implements Callable<Integer>{

        @Override
        public Integer call() throws Exception {
            System.out.println("running now");
            return new Long(System.currentTimeMillis()).intValue();
        }
    }

}
