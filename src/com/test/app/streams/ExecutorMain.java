package com.test.app.streams;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.Callable;
import com.test.app.streams.ExecutorExample.TestCallable;

public class ExecutorMain {
    public static void main(String[] arr){
        ExecutorExample executorExample = new ExecutorExample();

        List<Callable<Integer>> callable = new ArrayList<>();
        TestCallable testCallable = executorExample.new TestCallable();
        callable.add(testCallable);
        TestCallable testCallable2 = executorExample.new TestCallable();
        callable.add(testCallable2);
        TestCallable testCallable3 = executorExample.new TestCallable();
        callable.add(testCallable3);

        executorExample.testCompletionService(callable);

        System.exit(1);
    }
}
