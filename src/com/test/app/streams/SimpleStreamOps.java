package com.test.app.streams;

import java.util.stream.Stream;

public class SimpleStreamOps {

    public void printUniqueSortedNumbers(Integer[] arrInt){
        System.out.println("Print Numbers");
        Stream.of(arrInt)
                .sorted()
                .distinct()
                .forEach(x-> System.out.println(x));
    }

    public void printDoubleNumbers(Integer[] arrInt){
        System.out.println("Print Double");
        Stream.of(arrInt)
                .map(x -> x*2)
                .sorted()
                .distinct()
                .forEach(x-> System.out.println(x));
    }

    public void findNumber(Integer[] arrInt, int val){
        System.out.println("Find Number: "+val);
        Stream.of(arrInt)
                .filter(x -> x.equals(val))
                .sorted()
                .forEach(x-> System.out.println("Found: "+x));
    }

    public void filterAndDoubleNumbers(Integer[] arrInt){
        System.out.println("Double and Filter Anything More Than 50");
        Stream.of(arrInt)
                .map(x -> x*2)
                .distinct()
                .filter(x -> x > 50)
                .sorted()
                .forEach(x-> System.out.println(x));
    }
}

